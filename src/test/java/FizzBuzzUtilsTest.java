import org.junit.Test;
import fizzbuzz.utils.FizzBuzzUtils;
import fizzbuzz.utils.Report;

import static org.junit.Assert.*;

public class FizzBuzzUtilsTest {

    private static final Integer FROM = 1;
    private static final Integer TO = 20;
    private static final Integer WRONG_FROM_1 = -1;
    private static final Integer WRONG_FROM_2 = 10;
    private static final Integer WRONG_TO = 0;
    private static final String TARGET = "1 2 alfresco 4 buzz fizz 7 8 fizz buzz 11 fizz alfresco 14 fizzbuzz 16 17 fizz 19 ";
    private static final String TARGET_ERROR = "Please select a interval starting from 0 to "+ Integer.MAX_VALUE;
    private static final String REPORT = "fizzbuzz: 1 buzz: 2 fizz: 4 integer: 10 alfresco: 2 ";

    @Test
    public void process_interval_OK() throws Exception {
        String resultStr = FizzBuzzUtils.processInterval(FROM, TO);
        assertEquals(resultStr, TARGET);
        assertEquals(REPORT, Report.printReport());

    }

    @Test
    public void process_interval_WRONG_from_minus_value() throws Exception {
        String resultStr = FizzBuzzUtils.processInterval(WRONG_FROM_1, TO);
        assertEquals(resultStr, TARGET_ERROR);
    }

    @Test
    public void process_interval_WRONG_from_greater_than_to() throws Exception {
        String resultStr = FizzBuzzUtils.processInterval(WRONG_FROM_2, WRONG_TO);
        assertEquals(resultStr, TARGET_ERROR);
    }

}
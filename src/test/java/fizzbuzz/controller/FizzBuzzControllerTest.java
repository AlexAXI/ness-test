package fizzbuzz.controller;

import fizzbuzz.model.Interval;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FizzBuzzControllerTest {

    @LocalServerPort
    private int port;

    private static final String BASE_PATH = "/api/v1/fizzbuzz";

    @Test
    public void processList() throws Exception {
        Interval interval = new Interval();
        interval.setFrom(1);
        interval.setTo(20);

        //todo -> add matchers to check the body
        given().port(port).contentType("application/json").body(interval).when().post(BASE_PATH+"/run")
                .then()
                .statusCode(200);
    }

    @Test
    public void getReport() throws Exception {
        //todo -> add matchers to check the body
        given().port(port).when().get(BASE_PATH+"/getReport")
                .then()
                .statusCode(200);
    }

}
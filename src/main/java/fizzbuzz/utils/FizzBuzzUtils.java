package fizzbuzz.utils;

import fizzbuzz.model.enums.FizzBuzzEnum;

public class FizzBuzzUtils {


    public static String processInterval(Integer from, Integer to){
        StringBuilder result = new StringBuilder();
        Report.flushReport();

        if(from < to && from >=0 && to <= Integer.MAX_VALUE) {
            for(int i=from; i<to; i++){
                result.append(calculateItem(i));
                result.append(" ");
            }
        }else{
            result.append("Please select a interval starting from 0 to "+ Integer.MAX_VALUE);
        }

        return result.toString();
    }

    private static String calculateItem(Integer input){
        String result = input.toString();

        if(result.contains("3")) {
            Report.increaseReport(FizzBuzzEnum.alfresco);
            return "alfresco";
        }

        if(input % 15 == 0) {
            result = FizzBuzzEnum.fizzbuzz.toString();
            Report.increaseReport(FizzBuzzEnum.fizzbuzz);
            return result;
        }

        if(input % 3 == 0){
            result = FizzBuzzEnum.fizz.toString();
            Report.increaseReport(FizzBuzzEnum.fizz);
            return result;
        }

        if(input % 5 == 0){
            result = FizzBuzzEnum.buzz.toString();
            Report.increaseReport(FizzBuzzEnum.buzz);
            return result;
        }

        Report.increaseReport(FizzBuzzEnum.integer);
        return result;
    }


}

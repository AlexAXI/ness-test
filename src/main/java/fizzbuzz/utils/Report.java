package fizzbuzz.utils;

import fizzbuzz.model.enums.FizzBuzzEnum;

import java.util.HashMap;
import java.util.Map;

public class Report {

    private static final Map<FizzBuzzEnum, Integer> report = new HashMap<>();

    private Report(){
        initReport();
    }

    public static Map<FizzBuzzEnum, Integer> getReport(){
        return report;
    }

    private static void initReport(){
        report.put(FizzBuzzEnum.alfresco, 0);
        report.put(FizzBuzzEnum.buzz, 0);
        report.put(FizzBuzzEnum.fizz, 0);
        report.put(FizzBuzzEnum.fizzbuzz, 0);
        report.put(FizzBuzzEnum.integer, 0);
    }

    public static void increaseReport(FizzBuzzEnum type){
        report.merge(type, 1, Integer::sum);
    }

    public static String printReport(){
        StringBuilder print = new StringBuilder();
        report.forEach((k,v) -> {
            print.append(k);
            print.append(": ");
            print.append(v);
            print.append(" ");
        });

        return print.toString();
    }

    public static void flushReport(){
        report.clear();
    }
}

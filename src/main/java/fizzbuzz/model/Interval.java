package fizzbuzz.model;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class Interval {

    @NotNull
    @Min(value = 0)
    private Integer from;

    @NotNull
    @Max(value = Integer.MAX_VALUE)
    private Integer to;
}

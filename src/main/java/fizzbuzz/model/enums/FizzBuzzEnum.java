package fizzbuzz.model.enums;

public enum FizzBuzzEnum {
    fizz,
    buzz,
    fizzbuzz,
    alfresco,
    integer
}

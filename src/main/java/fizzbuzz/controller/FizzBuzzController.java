package fizzbuzz.controller;

import fizzbuzz.model.Interval;
import fizzbuzz.model.enums.FizzBuzzEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import fizzbuzz.service.impl.FizzBuzzService;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/fizzbuzz")
public class FizzBuzzController {

    private FizzBuzzService service;

    @Autowired
    public FizzBuzzController(FizzBuzzService service) {
        this.service = service;
    }

    @PostMapping("/run")
    public ResponseEntity<String> processList(@Validated @RequestBody Interval input){
        String response = service.process(input);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/getReport")
    public ResponseEntity<Map<FizzBuzzEnum, Integer>> getReport(){
        return new ResponseEntity<>(service.getReport(), HttpStatus.OK);
    }
}

package fizzbuzz.service;

import fizzbuzz.model.Interval;
import fizzbuzz.model.enums.FizzBuzzEnum;

import java.util.Map;


public interface FizzBuzz {

    String process(Interval interval);

    Map<FizzBuzzEnum, Integer> getReport();
}

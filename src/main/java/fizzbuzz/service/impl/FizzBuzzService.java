package fizzbuzz.service.impl;

import fizzbuzz.model.Interval;
import fizzbuzz.model.enums.FizzBuzzEnum;
import fizzbuzz.utils.Report;
import org.springframework.stereotype.Service;
import fizzbuzz.service.FizzBuzz;
import fizzbuzz.utils.FizzBuzzUtils;

import java.util.Map;

@Service
public class FizzBuzzService implements FizzBuzz {

    @Override
    public String process(Interval interval) {
        return FizzBuzzUtils.processInterval(interval.getFrom(), interval.getTo());
    }

    @Override
    public Map<FizzBuzzEnum, Integer> getReport() {
         return Report.getReport();
    }
}
